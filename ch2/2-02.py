"""
Cracking the Coding Interview Chapter 2 Question 2.2 solution
Remove Dups: Write an algorithm to return the kth to last element of a
singly linked list.
"""

import linkedlist
        
# Removes all duplicates from a linked list.
def kth_to_last(head):
    k_before = None
    current = head
    i = 0

    while current.next is not None:
        if i == k:
            k_before = head
        current = current.next
        if k_before:
            k_before = k_before.next
        i += 1
    return k_before
