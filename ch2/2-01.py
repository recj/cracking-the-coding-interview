"""
Cracking the Coding Interview Chapter 2 Question 2.1 solution
Remove Dups: Write code to remove duplicates from an unsorted linked list.
"""

import linkedlist
        
# Removes all duplicates from a linked list.
def remove_dups(head):
    prev = None
    current = head
    items = set()

    # If the list has only one node, it can't have any duplicates.
    if head.next is None:
        return start
    
    while current.next is not None:
        #If the current node is a duplicate, remove it from the list.
        if current.value in items:
            if current.prev:
                prev.next = current.next
        else:
            items.add(current)
        current = current.next

    return head
