"""
Question 2.3 Delete a node in the middle of a linked list.
"""

import linkedlist

def delete_middle_node(node):
    node.value = node.next.value
    node.next = node.next.next
    return
