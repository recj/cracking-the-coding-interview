"""
Assuming existence of is_substring, check if s2 is a rotation of s1
using only one call to is_substring.
"""

def is_rotation(s1: str, s2: str) -> str:
    if len(s2) > len(s1):
        return False
    return is_substring(s1 + s1, s2)
