def compress(s):
    prev_char = None
    count = 0
    s2 = ""
    s += " "
    for c in s:
        if not prev_char:
            prev_char = c
        if c == prev_char:
            count += 1
            prev_char = c
        else:
            s2 += prev_char + str(count)
            prev_char = c
            count = 1
    return s2
            
