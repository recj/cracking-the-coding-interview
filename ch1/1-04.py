def is_perm_of_palindrome(s):
    freq = {}

    s = str.lower(s.replace(" ", ""))
    for c in s:
        if c not in freq:
            freq[c] = 1
        else:
            freq[c] += 1

    even_length = len(s) % 2 == 0
    
    for char, frequency in freq.items():
       if frequency % 2 != 0:
           if not even_length:
               
               even_length = True
               continue
           else:
               return False
    return True


if __name__ == '__main__':
    assert(is_perm_of_palindrome("Tact Coa"))
