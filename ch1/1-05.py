def check_replaced(s1, s2):
    if len(s1) == len(s2):
        if s1 == s2:
            return True
        else:
            for c1, c2 in s1, s2:
                if c1 != c2  and not one_replaced:
                    one_edit = True
                elif c1 != c2:
                    return False
            return True

def check_added(s1, s2):
    one_edit = False
    
    i = 0
    j = 0
    while i < len(s1) and j < len(s2):
        if s2[j] != s1[i] and not one_edit:
            j += 1
            one_edit = True
        elif s2[j] != s1[i]:
            return False
        i += 1
        j += 1
    return True

def one_away(s1: string, s2: string) -> bool:
    # Checks if one character was replaced.

    #Check if one character was added.
    elif len(s2) == len(s2) + 1:
        return check_added(s1, s2)
        

    # Check if one character was removed.
    elif len(s2) == len(s1) - 1:
        i = 0
        while i < len(s1):
            pass
    else:
        return False

if __name__ == '__main__':
    assert(one_away("foo", "foo"))
    assert(not one_away("foo", "bar"))
    assert(one_away("foo", "fo"))
    assert(not one_away("foobarfoo", "foo"))
    assert(one_away("foo1", "foo2"))
    
           
