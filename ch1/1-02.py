
"""
Returns whether s2 is a permutation of s1
"""
def check_perm(s1, s2):
    chars = {}

    for c in s1:
        if c not in chars:
            chars[c] = 1
        else:
            chars[c] += 1

    for c in s2:
        if c not in chars:
            return False
        elif chars[c] > 0:
            chars[c] -= 1
        else:
            return False
    return True

if __name__ == '__main__':
    assert(check_perm("racecar", "racecar"))
    assert(check_perm("abc", "cb"))
    assert(check_perm("abc", "ba"))
    assert(check_perm("abc", "a"))
    assert(not check_perm("abc", "d"))
