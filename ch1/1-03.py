def urlify(s, n):
    s2 = ""
    for c in s:
        if c == " ":
            s2 += "%20"
        else:
            s2 += c

    return s2

if __name__ == '__main__':
    assert(urlify("Mr John Smith", 13) == "Mr%20John%20Smith")
        
