"""
Algorithm such that if an element in an mxn matrix is 0, its entire row and column are set to 0.
"""

# Set ith row in matrix to 0.
def set_row(m, i):
    for j, row in enumerate(m):
        if i == j:
            for elt in m[i]:
                elt = 0

# Set ith column in matrix to 0.
def set_col(m, i):
    for row in m:
        row[i] = 0

# Zeroes appropriate rows and columns in matrix.
def zero_matrix(m):
    m2 = m
    for i in len(m):
        for j in len(m[0]):
            if m[i][j] == 0:
                set_row(m2, i)
                set_col(m2, j)

    return m2
