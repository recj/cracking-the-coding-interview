
def is_unique(s):
    chars = set()
    
    for c in s:
        if c not in chars:
            chars.add(c)
        else:
            return False
    return True


if __name__ == '__main__':
    assert(not is_unique("AAAAA"))
    assert(is_unique("car"))
    assert(is_unique("bat"))
    assert(not is_unique("racecar"))
    
